﻿#pragma strict

var followTarget: GameObject;

function Start () {

}

function Update () {
    transform.position.x = followTarget.transform.position.x;
    transform.position.y = followTarget.transform.position.y;
    
//    transform.rotation = followTarget.transform.rotation;

	if (Input.GetKey(KeyCode.Space))
	{
		var tails = GameObject.FindGameObjectsWithTag('comet_tail');
		for (var tail in tails)
		{
			tail.transform.position.x = Random.Range(-34.0, 34.0);
			tail.transform.position.y = Random.Range(-19.0, 19.0);
		}
	}
}