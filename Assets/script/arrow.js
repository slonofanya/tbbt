﻿#pragma strict

var player: Transform;
var target: Transform;

function Start () {

}

function FixedUpdate () 
{
	// Gets a vector that points from the player's position to the target's.
	var heading = target.position - player.position;
	var distance = heading.magnitude;
//	Debug.Log(distance);

	var camera : Camera = GameObject.Find('MainCamera').GetComponent('Camera');
	    
    var c_hight = camera.orthographicSize;
    var pivot_angle = Mathf.Atan(c_hight / (c_hight * camera.aspect / 2));
    
    var arrow_distance = c_hight;
	if (Mathf.Abs(transform.rotation.z) > 1 - pivot_angle && Mathf.Abs(transform.rotation.z) < pivot_angle)
		arrow_distance *= camera.aspect;
	
	if (distance > arrow_distance)
	{
	    lookAtTarget(target);

	    transform.position = player.position;
	    transform.Translate(new Vector2(0, arrow_distance - 1.5));
    }
    else
    {
		hide();
    }
}

private function lookAtTarget(target: Transform)
{
	if (!target)
		return;
		
	transform.LookAt(target);

	// little gap
	if (transform.rotation.x < 0 && transform.rotation.y < 0 || transform.rotation.x > 0 && transform.rotation.y < 0)
		transform.Rotate(new Vector3(0, 0, 90));
	else
		transform.Rotate(new Vector3(0, 0, -90));
			
	transform.rotation.x = transform.rotation.y = 0;
}

function hide ()
{
	transform.position.x = -10000000;
}