#pragma strict

var target: Transform;
var max_distance = 1;
var moveSpeed = 1;

function Start ()
{
}

function Update ()
{
    var distance = Vector2.Distance(target.position, transform.position);
    
    if (distance >= max_distance)
    {
        transform.position = Vector2.MoveTowards(transform.position, target.position, moveSpeed * Time.deltaTime);
//        transform.position = transform.forward * moveSpeed * Time.deltaTime;
    }
    
    var diff = target.position - transform.position;
    //normalize difference
    diff.Normalize();
    
    //calculate rotation
    var rotZ = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg - 90;
    //apply to object
    transform.rotation = Quaternion.Euler(0f, 0f, rotZ);
}