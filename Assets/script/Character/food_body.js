﻿#pragma strict

function Start () {
	setRandomPosition();
}

function Update () {
}

function OnTriggerEnter2D(other: Collider2D) {
    if(other.gameObject.name == 'comet_header') {
    	for (var i=0;i<10;i++)
    		comet_headerCollider(other);
    		
		setRandomPosition();
    }
}

private function comet_headerCollider(other: Collider2D)
{
	var comet_footer = GameObject.Find("comet_footer");
	var footer_tail_script = comet_footer.GetComponent(tail);
	
	var new_tail = Instantiate(footer_tail_script.target);
	var new_tail_script = new_tail.gameObject.GetComponent(tail);
	
	new_tail_script.target = footer_tail_script.target;
	footer_tail_script.target = new_tail;
	
	new_tail_script.incTailIndex();
}

public function setRandomPosition()
{
	transform.position.x = Random.Range(-34.0, 34.0);
	transform.position.y = Random.Range(-19.0, 19.0);
}