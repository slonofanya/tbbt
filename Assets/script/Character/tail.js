#pragma strict

var target: Transform;
var max_distance: float = 0.55;
var tail_index: int = 0;

private var defult_name = 'comet_body';

function Start () 
{
}

function Update() 
{
	lookAtTarget(); // смотрим на цель
	moveToTarget(); // двигаем хвост
}

private function moveToTarget()
{
	if (!target)
		return;

    var direction = (target.position - transform.position);
    var distance = direction.magnitude;

    if (distance >= max_distance)
        transform.position += direction.normalized * (distance - max_distance) / 1;
}

private function lookAtTarget()
{
	if (!target)
		return;
		
	transform.LookAt(target);

	// little gap
	if (transform.rotation.x < 0 && transform.rotation.y < 0 || transform.rotation.x > 0 && transform.rotation.y < 0)
		transform.Rotate(new Vector3(0, 0, 90));
	else
		transform.Rotate(new Vector3(0, 0, -90));
			
	transform.rotation.x = transform.rotation.y = 0;
}

public function incTailIndex()
{
	++tail_index;
	gameObject.name = defult_name + '_' + tail_index;
}