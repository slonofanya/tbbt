#pragma strict

var speed = 1.0;
var max_speed = 0.1;
var min_speed = 0;
var rotationSpeed = 1.0;

private var _DEAD_ZONE = 0.1f;
private var move = Vector3.zero;
private var angle = Vector3.zero;
private var player : Transform;

function Start() {
	player = gameObject.transform;
}

function FixedUpdate () {
    var translation = Input.GetAxis("Vertical");
	var rotation = Input.GetAxis("Horizontal");
	
	Move(translation);
	Rotate(translation, rotation);
	
	test_animation(translation);
}

private function Move(translation: float) {
    if (translation > _DEAD_ZONE && move.y < max_speed)
        move += new Vector3(0, translation * speed / 100);

	if (translation < -_DEAD_ZONE && move.y >= min_speed){
    	move -= new Vector3(0, -translation * speed / 2 / 100);
    	
//    	if(move.x < Vector3.up.y)
//    		move = Vector3.zero;
    }
    
    player.Translate(move);
}

private function Rotate(translation: float, rotation: float) {
 	angle = Vector3.zero;
	
    if (rotation)
		angle += new Vector3(0, 0, rotation * rotationSpeed);
		
    player.Rotate(-angle);
}

private function test_animation(translation: float)
{
//   if (translation > 0.2)
//       animation.CrossFade ("test");
//   else
//      animation.CrossFade ("idle");
}