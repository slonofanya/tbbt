#pragma strict

function Start () {

}

var speed : float = 6.0;
var rotateSpeed : float = 6.0;

private var moveDirection : Vector3 = Vector3.zero;

function Update() {
    var controller : CharacterController = GetComponent(CharacterController);
    
    moveDirection = transform.TransformDirection(Vector3(0, Input.GetAxis("Vertical"), 0)) * speed;
    
    var rotateDirection = -Input.GetAxis("Horizontal") * Time.deltaTime * speed * rotateSpeed;
    
    transform.Rotate(0, 0, rotateDirection);
    
    controller.Move(moveDirection * Time.deltaTime);
}